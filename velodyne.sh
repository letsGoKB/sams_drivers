#!/bin/bash
source $(pwd)/devel/setup.bash
rosrun velodyne_driver velodyne_node
roslaunch velodyne_pointcloud VLP32C_points.launch
